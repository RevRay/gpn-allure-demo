package ru.gpn.alluredemo;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

public class SyntheticPageObject {
//    @Step("Пользователь логинится {0} {1}")
//    @Step("Пользователь логинится {user} {pass}")
    @Step("Пользователь логинится с учетными данными: {user} {pass}")
    public void login(String user, String pass){
    }
    @Step("Закрыл всплывающее окно")
    public void acceptAlert(){
        captureLogs();
    }
    @Step("Ввел поисковый запрос")
    public void enterSearchQuery(){}

    @Step("Кликнул пункт меню")
    public void clickMenu(){
        takeScreenshot();
    }
    @Step("Вышел из системы")
    public void logout(){}

    @Attachment
    public String captureLogs() {
        return "my sample logs";
    }

    @Attachment(value = "scr", type = "image/png") //MIME
    public byte[] takeScreenshot() {
        return new byte[]{0, 100, 20};
    }




}
