package ru.gpn.alluredemo;

import io.qameta.allure.*;
import org.testng.annotations.Test;

public class SimpleTests {

    @Description("Позитивный сценарий покупки пиццы за биткоины")
    @Severity(SeverityLevel.BLOCKER)
    @Epic("Осуществление платежей в приложении")
    @Features({
            @Feature("Оплата заказа криптовалютой)"),
            @Feature("Оплата заказа криптовалютой)"),
            @Feature("Оплата заказа криптовалютой)")
    })
    @Story("Оплата товара с помощью биткоинов")
    @Test(description = "Позитивный сценарий когда на криптокошельке достаточно средств")
    public void test1() {
        System.out.println("test1");
    }

    @Epic("Секьюрное общение с приложение")
    @Feature("OAuth 2.0 аутентификация")
    @Story("Аутентификация с учеткой Google")
    @Test(description = "Негативный сценарий с деактивированной учеткой")
    @Description("Негативный сценарий когда токен был некорректно сгененрирован")
    @Severity(SeverityLevel.MINOR)
    public void test2() {
        System.out.println("test1");
    }

    @Test
    @Severity(SeverityLevel.TRIVIAL)
    public void test3() {
        SyntheticPageObject syntheticPageObject = new SyntheticPageObject();
        syntheticPageObject.login("Вася", "securepass");
        syntheticPageObject.acceptAlert();
        syntheticPageObject.clickMenu();
        syntheticPageObject.logout();

        Allure.label("", "");
    }

    @Test
    public void test4() {
        System.out.println("test1");
    }

    @Test
    public void test5() {
        throw new AssertionError("flag");
    }
    @Test
    public void test6() {
        throw new ArithmeticException();
    }

    @Test(enabled = false)
    public void test7() {
        throw new ArithmeticException();
    }

    //@Epic -> @Feature -> @Story

//    @Test(description = "my flaky test",
//            retryAnalyzer = MyRetryAnalyzer.class)
    @Test(description = "my flaky test")
    public void flakyTest() {
        double random = Math.random(); //0 , 1

        if (random > 0.4) {
            throw new AssertionError();
        }
    }

    @Test(description = "my flaky test 2")
    public void flakyTest2() {
        double random = Math.random(); //0 , 1

        if (random > 0.4) {
            throw new AssertionError();
        }
    }

    @Epic("Секьюрное общение с приложение")
    @Feature("OAuth 2.0 аутентификация")
    @Story("Аутентификация с учеткой Google")
    @Test(description = "Негативный сценарий с деактивированной учеткой")
    @Description("Негативный сценарий когда токен был некорректно сгененрирован")
    @TmsLink("SM-103")
    @Issue("ATBUG-1004")
    @Link("url:8080/shares/prices")
    public void testIntegrations() {
        System.out.println("test1");
    }
}
